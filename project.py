
data = input("Add array separate by comma, ex: 4,5,6,7: ")
result = int(input("Add number to find: "))
arrayNumber = data.split(",")

for index in range(len(arrayNumber)):
    number = int(arrayNumber[index])
    if number <= result:
        numberFind = result - number
        exists = str(numberFind) in arrayNumber[(index+1):]
        if exists:
            print("+ " + str(number) + "," + str(numberFind))
